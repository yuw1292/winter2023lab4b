import java.util.Scanner;

public class VirtualPetApp{
	public static void main(String[] args){
		
		Scanner reader = new Scanner(System.in);
		
		Fox[] skulk = new Fox[4];
		for (int i = 0; i < skulk.length; i++) {       
			System.out.println("Enter which subspecie of fox it is:");
			String specie = reader.nextLine();
			System.out.println("Enter the Scientific name of the fox:");
			String latinName = reader.nextLine();
			System.out.println("Enter around how many hours it stays active during a day:");
			int activeHours = Integer.parseInt(reader.nextLine());
			
			skulk[i] = new Fox(specie, latinName, activeHours);
        } 
		System.out.println(skulk[3].getSpecie());
		System.out.println(skulk[3].getLatinName());
		System.out.println(skulk[3].getActiveHours());
		
		System.out.println("Update which subspecie of the last fox");
		String newSpecie = reader.nextLine();
		skulk[3].setSpecie(newSpecie);
		
		System.out.println(skulk[skulk.length-1].getSpecie());
		System.out.println(skulk[skulk.length-1].getLatinName());
		System.out.println(skulk[skulk.length-1].getActiveHours());
	}
}