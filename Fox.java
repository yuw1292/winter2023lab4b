import java.util.Random;

public class Fox{
    private String specie;
    private String latinName;
    private int activeHours;
	private int groupSize;
	
	public Fox(String specie, String latinName, int activeHours) {
		this.specie = specie;
		this.latinName = latinName;
		this.activeHours = activeHours;
	}
	public void bePresented(){
		System.out.println("This fox, is a " + specie + " fox that is also known as the " + latinName + ", is active for " + activeHours + " hours per day.");
	}
	
	 public void createGroup() {
        Random random = new Random();
        groupSize = random.nextInt(10) + 1;
        System.out.println("This fox is part of a group of " + groupSize + " foxes.");
    }
	public String getSpecie() {
		return this.specie;
	}
	public String getLatinName() {
		return this.latinName;
	}
	public int getActiveHours() {
		return this.activeHours;
	}
	public int getGroupSize() {
		return this.groupSize;
	}
	public void setSpecie(String specie) {
		this.specie = specie;
	}
}
